# 在openeuler22.03上适配K8s

#### 介绍
K8s（Kubernetes）是一个开源的容器编排平台，用于自动化容器化应用程序的部署、扩展和管理，支持大规模可伸缩、应用容器化管理，并提供了服务发现、负载均衡、自动伸缩、存储管理等一系列功能，极大地简化了容器化应用的运维工作。


#### 安装前准备

1.  主机名配置
    由于本次使用3台主机完成kubernetes集群部署，其中1台为master节点,名称为k8s-master01;其中2台为node节点，名称分别为：k8s-node1及k8s-node2

```
master节点
# hostnamectl set-hostname k8s-master01 && bash
node1节点
# hostnamectl set-hostname k8s-node1 && bash
node2节点
# hostnamectl set-hostname k8s-node2 && bash
```

2.  主机名与IP地址解析
    所有集群主机均需要进行配置。
```
cat >> /etc/hosts <<EOF
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
172.16.200.90 k8s-master01
172.16.200.91 k8s-node1
172.16.200.92 k8s-node2
EOF
```
3.  关闭SWAP分区

```
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
```
4.  防火墙配置
    所有主机均得配置
```
#关闭现有防火墙firewalld
systemctl disable firewalld
systemctl stop firewalld
```

5.  SELINUX配置
    所有主机均得配置
```
sed -ri 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
```
6.  配置内核转发及网桥过滤
    所有主机均得配置
```
#开启内核路由转发
sed -i 's/net.ipv4.ip_forward=0/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
```
```
#添加网桥过滤及内核转发配置文件

cat <<EOF >/etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
vm.swappiness = 0
EOF
```

```
配置加载br_netfilter模块

cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

#加载br_netfilter overlay模块
modprobe br_netfilter
modprobe overlay 
```

```
#查看是否加载
lsmod | grep br_netfilter
br_netfilter           22256  0
```

```
使用默认配置文件生效
sysctl -p 
#使用新添加配置文件生效
sysctl -p /etc/sysctl.d/k8s.conf 
```
7. 安装ipset及ipvsadm
    所有主机均得操作

```
安装ipset及ipvsadm
yum -y install ipset ipvsadm
```

```
#配置ipvsadm模块加载方式.添加需要加载的模块

cat > /etc/sysconfig/modules/ipvs.module <<EOF
modprobe -- ip_vs
modprobe -- ip_vs_sh
modprobe -- ip_vs_rr
modprobe -- ip_vs_wrr
modprobe -- nf_conntrack
EOF
```

```
授权、运行、检查是否加载
chmod 755 /etc/sysconfig/modules/ipvs.module &&  /etc/sysconfig/modules/ipvs.module

查看对应的模块是否加载成功
# lsmod | grep -e ip_vs -e nf_conntrack_ipv4
```
#### 安装docker
    略
#### 安装container容器

1.  下载 containerd 包

```
wget https://github.com/containerd/containerd/releases/download/v1.6.6/cri-containerd-cni-1.6.6-linux-amd64.tar.gz
#解压
tar zxvf cri-containerd-cni-1.6.6-linux-amd64.tar.gz -C /
```
2.  创建容器目录

```
mkdir /etc/containerd
```
3.  生成容器配置文件

```
containerd config default  >> /etc/containerd/config.toml
```
4. 配置systemdcgroup 驱动程序

```
vim /etc/containerd/config.toml
```

```
[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
  ...
  [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
    SystemdCgroup = true 
```
5.  修改sandbox (pause) image地址

```
vim /etc/containerd/config.toml
```

```
[plugins."io.containerd.grpc.v1.cri"]
  sandbox_image = "registry.aliyuncs.com/google_containers/pause:3.2"
```
6. 更新runc，因为cri-containerd-cni-1.6.6-linux-amd64.tar.gz的runc二进制文件有问题

```
wget https://github.com/opencontainers/runc/releases/download/v1.1.3/runc.amd64
mv runc.amd64 /usr/local/sbin/runc
chmod +x /usr/local/sbin/runc
```
7.  启动containerd服务
```
systemctl start containerd
systemctl enable containerd
```

#### kubernetes 1.24.2 集群部署
1.  kubernetes YUM源准备

```
cat >/etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

yum clean all && yum makecache
```
2.  集群软件安装
    所有节点均安装

```
yum install  kubelet-1.24.2 kubeadm-1.24.2 kubectl-1.24.2
systemctl enable kubelet --now
```
3. 配置kubelet

```
cat <<EOF > /etc/sysconfig/kubelet
KUBELET_EXTRA_ARGS="--cgroup-driver=systemd"
EOF
```
4. 集群初始化
    只在主节点执行

```
kubeadm init --kubernetes-version v1.24.2 --pod-network-cidr=10.244.0.0/16 --image-repository=gcmirrors
```
5. 集群应用客户端管理集群文件准备

```
[root@k8s-master01 ~]# mkdir -p $HOME/.kube
[root@k8s-master01 ~]# cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
[root@k8s-master01 ~]# chown $(id -u):$(id -g) $HOME/.kube/config
[root@k8s-master01 ~]# ls /root/.kube/
config
```
```
export KUBECONFIG=/etc/kubernetes/admin.conf
```

6.  集群工作节点添加

```
[root@k8s-node1 ~]# kubeadm join 172.16.200.90:6443 --token 4e2uez.vzy37zl8btnd6fif \
>         --discovery-token-ca-cert-hash sha256:3cf872560fe7f67fcc3f28fdbe3ffb84fae6348cf56898ed7e7a164cf562948e
```

```
[root@k8s-node2 ~]# kubeadm join 172.16.200.90:6443 --token 4e2uez.vzy37zl8btnd6fif \
>         --discovery-token-ca-cert-hash sha256:3cf872560fe7f67fcc3f28fdbe3ffb84fae6348cf56898ed7e7a164cf562948e
```
#### 集群网络准备

1. 分别在 Master 和 Worker 节点上执行如下命令，下载 calico 容器镜像。

```
docker pull calico/cni:v3.14.2-arm64
docker pull calico/node:v3.14.2-arm64
docker pull calico/kube-controllers:v3.14.2-arm64
docker pull calico/pod2daemon-flexvol:v3.14.2-arm64
```
2.  分别在 Master 和 Worker 节点上执行如下命令，修改已下载的镜像标签。

```
docker tag calico/cni:v3.14.2-arm64 calico/cni:v3.14.2
docker tag calico/node:v3.14.2-arm64 calico/node:v3.14.2
docker tag calico/kube-controllers:v3.14.2-arm64 calico/kube-controllers:v3.14.2
docker tag calico/pod2daemon-flexvol:v3.14.2-arm64 calico/pod2daemon-flexvol:v3.14.
```
3.  执行如下命令，查看是否成功打上 calico 标签

```
docker images | grep calico
```
4.  分别在 Master 和 Worker 节点上执行如下命令，删除旧镜像。

```
docker rmi calico/cni:v3.14.2-arm64
docker rmi calico/node:v3.14.2-arm64
docker rmi calico/kube-controllers:v3.14.2-arm64
docker rmi calico/pod2daemon-flexvol:v3.14.2-arm64
```
5.  在 Master 节点上执行如下命令，下载 yaml 文件。

```
wget https://docs.projectcalico.org/v3.14/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml --no-check-certificate
```
6.  Master 节点上执行如下命令，部署 calico。

```
kubectl apply -f calico.yaml
```
7.  在 Master 节点上执行如下命令，查看节点状态，状态为 Ready 即表明安装成功。

```
kubectl get nodes
```
